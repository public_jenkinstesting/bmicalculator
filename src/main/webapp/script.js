function calculateBMI() {
  // Get height and weight inputs
  var heightInput = document.getElementById("height").value;
  var weightInput = document.getElementById("weight").value;

  // Calculate BMI
  var heightMeters = heightInput / 100;
  var bmi = weightInput / (heightMeters * heightMeters);

  // Display BMI result
  var result = document.getElementById("result");
  result.innerHTML = "Your BMI is " + bmi.toFixed(1);
}
/**
 * 
 */